package in.kotlinindia

import io.javalin.Javalin

fun main(args: Array<String>) {
    val port: Int = args.get(0).toInt()
    val app = Javalin.create().start(port)
    app.get("/") { ctx -> ctx.result("kt hub World") }
}
